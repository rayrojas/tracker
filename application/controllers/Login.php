<?php
class Login extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function v_404()
    {

        $this->load->view('templates/header', array());
        $this->load->view('v404');
        $this->load->view('templates/footer');
    }    
    public function index()
    {
        $this->form_validation->set_rules('username' ,'Username', 'required');
        $this->form_validation->set_rules('password' ,'Password', 'callback_verify');
        if($this->form_validation->run() == false)
        {
            $data = array();
            // $data['main_title'] = 'Biblioteca';
            // $data['title2'] = 'Registro';

            $this->load->view('templates/header', $data);
            $this->load->view('login');
            $this->load->view('templates/footer');
        }
        else
        {
            redirect('incident/index');
        }
    }
    public function logout()
    {
        $user = $this->session->userdata('user_data');
        if (isset($user)) {
            $this->session->unset_userdata('user_data');
        } else {
        }
        redirect('incident/index');
    }
    public function register()
    {
        $this->form_validation->set_rules('username' ,'Username', 'required');
        $this->form_validation->set_rules('password' ,'Password', 'callback_verify');
        if($this->form_validation->run() == false)
        {
            $data = array();

            $this->load->view('templates/header', $data);
            $this->load->view('register');
            $this->load->view('templates/footer');
        }
        else
        {
            redirect('incident/index');
        }
    }
    public function verify()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if($this->login_model->login($username, $password))
        {
            redirect('incident/index');
        }
        else
        {
            $this->session->set_flashdata('authError', 'Usuario o contraseña invalida.');
            // $this->form_validation->set_message('verify','Contraseña incorrecta');
            redirect('login');
        }
    }
}