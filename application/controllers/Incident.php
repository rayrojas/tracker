<?php
class Incident extends CI_controller
{

    
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function concluir_db() {
  		if(!is_logged_in()) {
  			redirect('login');
  		}
      $this->form_validation->set_rules('sustent' ,'Sustentar', 'callback_conclusionverify');
      if($this->form_validation->run() == false)
      {
      }
    }

    public function sustent_db() {
  		if(!is_logged_in()) {
  			redirect('login');
  		}
      $this->form_validation->set_rules('sustent' ,'Sustentar', 'callback_sustentverify');
      if($this->form_validation->run() == false)
      {
      }
    }
    public function observados() {
  		if(!is_logged_in()) {
  			redirect('login');
  		}
  		
      if ( !is_analista() ) {
        $this->session->set_flashdata('generalError', 'No tienes permisos para acceder a esta lista.');
        redirect('incident/index');
      }  		
  		
      $DNI = $this->input->get('dni', '---');
      if ( strlen($DNI) <= 0 ) {
        $DNI = '---';
      }
  		$incidents = $this->incident_model->getClosed(array("dni"=>$DNI));

      $registros = array();
      
      foreach ($incidents->result_array() as $row){
        $tmp = array();
        $tmp["id"] = $row['id'];
        $tmp["code"] = $row['code'];
        $tmp["entity"] = $row['entity'];
        if ($row['status'] == 'CLOSE'  ) {
          $tmp["status"] = 'Cerrado';
        } else if ($row['status'] == 'SUSTENTED'  ) {
          $tmp["status"] = 'Sustentado';
        } else {
          $tmp["status"] = 'Abierto';
        }
        
        $tmp["incidence_date"] = date("Y-m-d", strtotime($row['incidence_date']));
        
        $tmp["system"] = $row['system'];
        
        $tmp["user_staff_id"] = $row['user_staff_id'];
        $tmp["user_gestor_id"] = $row['user_gestor_id'];
        
        $usero = $this->user_model->getUserOrDefault($tmp["user_staff_id"]);
        $tmp["staff_object"] = $usero;

        $gestor = $this->user_model->getUserOrDefault($tmp["user_gestor_id"]);
        $tmp["gestor_object"] = $gestor;
        
        array_push($registros, $tmp);
      }
      
      $data['rows'] = $registros;
      $this->load->view('templates/header', $data);
      $this->load->view('incident/observados');
      $this->load->view('templates/footer');
    }
    public function detail($_id = null)
    {
  		if(!is_logged_in()) {
  			redirect('login');
  		}
  		
      if ($_id == null ) {
        $this->session->set_flashdata('generalError', 'El incidente no pudo ser encontrado.');
        redirect('incident/index');
      }
      
      $incident_one = $this->incident_model->getById($_id);
      if ($incident_one == null  ) {
        redirect('incident/index');
      }
      $user_one = $this->user_model->getUserOrDefault($incident_one->user_staff_id);
      $gestor_one = $this->user_model->getUserOrDefault($incident_one->user_gestor_id);
      
      $sustent_object = $this->incident_model->getSustentById($_id);
      
      $data = array('param_object' => $incident_one, 
        'user_object' => $user_one, 
        'gestor_object' => $gestor_one,
        'is_gestor' => is_gestor(),
        'is_analista' => is_analista(),
        'sustent_object' => $sustent_object);
      $this->load->view('templates/header', $data);
      $this->load->view('incident/detail');
      $this->load->view('templates/footer');
    }
    
    public function concluir($_id = null)
    {
  		if(!is_logged_in()) {
  			redirect('login');
  		}
      if ($_id == null ) {
        $this->session->set_flashdata('generalError', 'El incidente no pudo ser encontrado.');
        redirect('incident/index');
      }
      
      $incident_one = $this->incident_model->getById($_id);
      if ($incident_one == null  ) {
        $this->session->set_flashdata('generalError', 'El incidente no pudo ser encontrado.');
        redirect('incident/index');
      }
      
      if ($incident_one->status == 'CLOSE'  ) {
          $this->session->set_flashdata('incidentMsgConcluir', 
            'Esta incidencia ya esta cerrada.');
          redirect('incident/detail/'.$_id);
      }
      
      $user_one = $this->user_model->getUserOrDefault($incident_one->user_staff_id);
      $gestor_one = $this->user_model->getUserOrDefault($incident_one->user_gestor_id);
      
      $sustent_object = $this->incident_model->getSustentById($_id);
      if ($sustent_object == null  ) {
        $this->session->set_flashdata('generalError', 'El incidente no ha sido sustentado.');
        redirect('incident/index');
      }
      
      $data = array('param_object' => $incident_one, 
        'user_object' => $user_one, 
        'gestor_object' => $gestor_one,
        'is_gestor' => is_gestor(),
        'is_analista' => is_analista(),
        'sustent_object' => $sustent_object);

      $this->load->view('templates/header', $data);
      $this->load->view('incident/concluir');
      $this->load->view('templates/footer');
      
    }
    public function sustentar($_id = null)
    {
  		if(!is_logged_in()) {
  			redirect('login');
  		}
  		
      if ($_id == null ) {
        $this->session->set_flashdata('generalError', 'El incidente no pudo ser encontrado.');
        redirect('incident/index');
      }

      $incident_one = $this->incident_model->getById($_id);
      if ($incident_one == null  ) {
        $this->session->set_flashdata('generalError', 'El incidente no pudo ser encontrado.');
        redirect('incident/index');
      }      

      if ($incident_one->status == 'CLOSE'  ) {
          $this->session->set_flashdata('incidentMsgConcluir', 
            'Esta incidencia ya esta cerrada.');
          redirect('incident/detail/'.$_id);
      }

      $user_one = $this->user_model->getUserOrDefault($incident_one->user_staff_id);
      $gestor_one = $this->user_model->getUserOrDefault($incident_one->user_gestor_id);
      
      $sustent_object = $this->incident_model->getSustentById($_id);
      
      $data = array('param_object' => $incident_one, 
        'user_object' => $user_one, 
        'gestor_object' => $gestor_one,
        'is_gestor' => is_gestor(),
        'is_analista' => is_analista(),
        'sustent_object' => $sustent_object);      
      
      $this->load->view('templates/header', $data);
      $this->load->view('incident/sustent');
      $this->load->view('templates/footer');
      
    }
    public function index()
    {
  		if(!is_logged_in()) {
  			redirect('login');
  		}
  		
  		$incidents = null;
  		
  		if ( is_gestor() ) {
  		  $incidents = $this->incident_model->getByGestor(user_id(), user_entity());
  		}
      
  		if ( is_analista() ) {
  		  $incidents = $this->incident_model->filter();
  		}
  		
  		if ($incidents == null  ) {
        $this->session->set_flashdata('generalError', 
          'Lo sentimos pero usted no es Gestor o Analista. No posee permisos.');
  		  redirect('404');
  		}

      $registros = array();
      
      foreach ($incidents->result_array() as $row){
        $tmp = array();
        $tmp["id"] = $row['id'];
        $tmp["code"] = $row['code'];
        $tmp["entity"] = $row['entity'];
        if ($row['status'] == 'CLOSE'  ) {
          $tmp["status"] = 'Cerrado';
        } else if ($row['status'] == 'SUSTENTED'  ) {
          $tmp["status"] = 'Sustentado';
        } else {
          $tmp["status"] = 'Abierto';
        }
        
        $tmp["incidence_date"] = date("Y-m-d", strtotime($row['incidence_date']));
        
        $tmp["system"] = $row['system'];
        
        $tmp["user_staff_id"] = $row['user_staff_id'];
        $tmp["user_gestor_id"] = $row['user_gestor_id'];
        
        $usero = $this->user_model->getUserOrDefault($tmp["user_staff_id"]);
        $tmp["staff_object"] = $usero;

        $gestor = $this->user_model->getUserOrDefault($tmp["user_gestor_id"]);
        $tmp["gestor_object"] = $gestor;
        
        array_push($registros, $tmp);
      }
      
      $data['rows'] = $registros;
      $this->load->view('templates/header', $data);
      $this->load->view('incident/index');
      $this->load->view('templates/footer');
    }
    public function sustentverify()
    {
        $_id = $this->input->post('id');
        $sustent = $this->input->post('sustent');
        $invalid = false;
        if ( !isset($sustent) || strlen($sustent) <= 0) {
          $invalid = true;
        }
        if ( $invalid ) {
          $this->session->set_flashdata('incidentMsgConcluir', 
            'Por favor ingrese datos correctos.');          
          redirect('incident/sustent/'.$_id);
        } else {
          
          $row = $this->incidentdetail_model->saveOrUpdate(array(
            'incident_id' => $_id,
            'user_analista_id' => user_id(),
            'sustent' => $sustent,
            'conclusion' => '',
            'user_status' => 'UNBLOCKED',
            'record_date' => '2016-12-12',
          ));
          
          $incident_one = $this->incident_model->updateStatus($_id, 'SUSTENTED');
          
          $this->session->set_flashdata('incidentMsgConcluir', 
            'Datos actualizados.');
          redirect('incident/detail/'.$_id);
        }
    }
    public function conclusionverify()
    {
        $_id = $this->input->post('id');
        $_detail_id = $this->input->post('detail_id');
        $conclusion = $this->input->post('conclusion');
        $user_status = $this->input->post('user_status');
        $invalid = false;
        
        if ( !isset($conclusion) || strlen($conclusion) <= 0) {
          $invalid = true;
        }
        if ( !isset($user_status) || strlen($user_status) <= 0) {
          $invalid = true;
        }
        if ( $invalid ) {
          $this->session->set_flashdata('incidentMsgConcluir', 
            'Por favor ingrese datos correctos.');          
          redirect('incident/concluir/'.$_id);
        } else {
          
          $row = $this->incidentdetail_model->saveOrUpdate(array(
            'incident_id' => $_id,
            'user_analista_id' => user_id(),
            'conclusion' => $conclusion,
            'user_status' => $user_status,
            'record_date' => '2016-12-12',
          ));
          
          $incident_one = $this->incident_model->updateStatus($_id, 'CLOSE');
          
          $this->session->set_flashdata('incidentMsgConcluir', 
            'Datos actualizados.');
          redirect('incident/detail/'.$_id);
        }
        
        
    }
}