<?php
class Login_model extends CI_model
{
    public function __construct()
    {
        $this->load->database();
    }
    public function login($nombre, $password)
    {
        $query = $this->db->get_where('auth_user', array('username' => $nombre));
        if($query->num_rows() == 1)
        {
            $row=$query->row();
            if($password == $row->password)
            {
                $query_groups = $this->db->query("select id from auth_user_groups where group_id = 1 and user_id=".$row->id);
                $is_gestor = false;
                if ($query_groups->num_rows() >= 1) {
                    $is_gestor = true;
                }
                $query_groups = $this->db->query("select id from auth_user_groups where group_id = 2 and user_id=".$row->id);
                $is_analista = false;
                if ($query_groups->num_rows() >= 1) {
                    $is_analista = true;
                }
                $data = array(
                    'user_data'=>array(
                        'username'=>$row->username,
                        'id'=>$row->id,
                        'is_analista' => $is_analista,
                        'is_gestor' => $is_gestor,
                        'entity' => $row->entity
                    )
                );
                
                $this->session->set_userdata($data);
                return true;
            }
        }
        $this->session->unset_userdata('user_data');
        return false;
    }
}