<?php
class Incidentdetail_model extends CI_model
{

    public function __construct()
    {
        $this->load->database();
    }
    public function filter()
    {
        $query = $this->db->query("select * from incident_detail order by id desc");
        return $query;
    }
   public function getById($_id)
    {
        $query = $this->db->query("select * from incident_detail where id =".$_id);
        $row = $query->row();
        return $row;
    }
    public function saveOrUpdate($_data) {
        $query = $this->db->query("select * from incident_detail where incident_id =".$_data['incident_id']);
        if($query->num_rows() >= 1){
            $row = $query->row();
            $_s_sustent = $_data['sustent'];
            if ( $_s_sustent == null ) {
                $_s_sustent = $row->sustent;
            }
            $data = array(
                'sustent' => $_s_sustent,
                'conclusion' => $_data['conclusion'],
                'user_status' => $_data['user_status'],
                'record_date' =>$_data['record_date']
            );
            $this->db->update('incident_detail',$data,'id='.$row->id);
        } else {
            $this->db->insert('incident_detail', $_data);   
        }
    }
}