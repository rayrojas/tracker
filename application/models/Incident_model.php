<?php
class Incident_model extends CI_model
{
    public $blocking_date;
    public $_system;
    public $causes;
    public $detail;
    public $user_id;

    public function __construct()
    {
        $this->load->database();
    }
    public function filter()
    {
        $query = $this->db->query("select * from incident order by id desc");
        return $query;
    }
    public function getByGestor($id, $entity) {
        $query = $this->db->query("select * from incident where status in ('OPEN', 'SUSTENTED') and user_gestor_id=".$id." and entity='".$entity."' order by id desc");
        return $query;
    }
    public function getClosed($data) {
        if ( $data["dni"] == "---" ) {
        $query = $this->db->query("select * from incident as ii inner join incident_detail as i_d on ii.id = i_d.incident_id inner join auth_user as a_u on a_u.id = ii.user_staff_id where ii.status = 'CLOSE'  order by ii.id desc");    
        } else {
            $query = $this->db->query("select * from incident as ii inner join incident_detail as i_d on ii.id = i_d.incident_id inner join auth_user as a_u on a_u.id = ii.user_staff_id where ii.status = 'CLOSE' and a_u.username='".$data["dni"]."' order by ii.id desc");
        }
        
        return $query;
    }
    public function getSustentById($_id){
        $query = $this->db->query("select * from incident_detail where incident_id =".$_id);
        $row = $query->row();
        return $row;
    }
   public function getById($_id)
    {
        $query = $this->db->query("select * from incident where id =".$_id);
        $row = $query->row();
        return $row;
    }
    public function updateStatus($id, $_status) {
        $data = array(
            'status' => $_status
        );
        
        // $this->db->where('id', $id);
        // $this->db->update('incident', $data);
        $this->db->update('incident',$data,'id='.$id);

        // $this->db->set('status',$_status)
        //     ->where('id',$id)
        //     ->update('incident');        
        // // $this->db->update('incident', array('status' => $_status));
        
    }
    
}
