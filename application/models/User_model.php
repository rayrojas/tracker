<?php
class User_model extends CI_model
{
    public function __construct()
    {
        $this->load->database();
    }
    public function filter()
    {
        $query = $this->db->query("select * from auth_user order by id desc");
        return $query;
    }
    public function getUserOrDefault($_id) {
      $query = $this->getById($_id);

      if ($query->num_rows() == 1) {
        $o = $query->row();
        return array("username" => $o->username, 
          "full_name" => $o->first_name." ".$o->last_name, 
          "id" => $o->id);
      }
      return array("username" => "Desconocido", 
          "full_name" => "Desconocido", 
          "id" => 0);
    }
    public function getByUsername($_username)
    {
        $query = $this->db->query("select * from auth_user where username = '".$_username."'");
        return $query->row();
    }
   public function getById($_id)
    {
        return $this->db->query("select * from auth_user where id =".$_id);
    }
}