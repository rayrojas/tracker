<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
    <div class='fila'>
        <div class='columna-unica'>
            <?php if( $this->session->flashdata('authError') )
            {
              echo '<div style="border: 1px solid #ccc; padding: 10px; margin-bottom: 20px">';
               echo $this->session->flashdata('authError');
              echo '</div>';
            }?>
            <?php echo validation_errors(); ?>
            <?php echo form_open('login'); ?>
            <label>Nombre de usuario:</label>
            <input type="text" name="username" /><br/>
            <label>Contraseña:</label>
            <input type="password" name="password" />
            <button type="submit">Login</button>
            <?php echo form_close();?>
        </div>
    </div>