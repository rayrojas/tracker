<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
    <div class='fila'>
        <div class='columna-unica'>
            <?php if( $this->session->flashdata('authError') )
            {
              echo '<div style="border: 1px solid #ccc; padding: 10px; margin-bottom: 20px">';
              echo $this->session->flashdata('authError');
              echo '</div>';
            }?>
            <?php echo validation_errors(); ?>
            <?php echo form_open('login'); ?>
              <!-- USERNAME -->
              <div class="form-group">
                <label for='data_1'>Usuario</label>
                <input type="text" id='data_1' name="usuario" 
                  placeholder="Enter Username" required autofocus>
              </div>
              <!-- PASSWORD -->
              <div class="form-group">
                <label for='data_2'>Contraseña</label>
                <input type="password" id='data_2' name="passwd" 
                  placeholder="Enter Password" required>
              </div>
    
              <!-- TIPO DOCUMENTO -->
              <div class="form-group">
                <label for='data_3'>Documento</label>
                <input type="text" id='data_3' name="doc" placeholder="DNI or CE" required>
              </div>
              
              <!-- DOCUMENTO -->
              <div class="form-group">
                <label for='data_4'>Número de Documento</label>
                <input type="text" id='data_4' name="numdoc" placeholder="Número de DNI or CE" required>
              </div>
              
              <!-- NOMBRES -->
              <div class="form-group">
                <label for='data_5'>Nombres</label>
                <input type="text" id='data_5' name="nombres" placeholder="Apellidos y Nombres" required>
              </div>
              
              <!-- ENTIDAD -->
              <div class="form-group">
                <label for='data_6'>ID Entidad</label>
                <input type="text" id='data_6' name="entidad" placeholder="Entidad" required>
              </div>
              
              <!-- PERFIL -->
              <div class="form-group">
                <label for='data_7'>ID Perfil</label>
                <input type="text" id='data_7' name="perfil" placeholder="Perfil" required>
              </div>
              <div class="form-group">
                <button type="submit">Registrar</button>
              </div>
            <?php echo form_close();?>
        </div>
    </div>