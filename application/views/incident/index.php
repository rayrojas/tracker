<?php if( $this->session->flashdata('generalError') )
{
  echo '<div class="text-warning" style="width: 80%;border: 1px solid #ccc; padding: 10px; margin-bottom: 20px">';
   echo $this->session->flashdata('generalError');
  echo '</div>';
}?>
<table style="margin-bottom: 20px">
  <thead>
    <tr>
      <th><a href="<?php echo FULL_BASE_URL.'/incident/index'; ?>" >Incidencias</a></th>
      <th><a href="<?php echo FULL_BASE_URL.'/incident/observados'; ?>" >Observados</a></th>
    </tr>
  </thead>
</table>
<table>
  <thead>
    <tr>
      <th width="40">Codigo</th>
      <th width="40">Sistema</th>
      <th width="40">Entidad</th>
      <th width="40">Usuario</th>
      <th width="40">Fecha</th>
      <th width="40">Gestor</th>
      <th width="40">Estado</th>
      <th width="40">Accion</th>
    </tr>
  </thead>
  <tbody>
    <?php 
    foreach ($rows as $row){ ?>
    <tr>
      <td><?php echo $row['code']; ?></td>
      <td><?php echo $row['system']; ?></td>
      <td><?php echo $row['entity']; ?></td>
      <td><?php echo $row['staff_object']["full_name"]; ?></td>
      <td><?php echo $row['incidence_date']; ?></td>
      <td><?php echo $row['gestor_object']["full_name"]; ?></td>
      <td><?php echo $row['status']; ?></td>
      <td>
        <a href="<?php echo FULL_BASE_URL."/incident/detail/".$row["id"]; ?>" 
        class="button btn">></a>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>