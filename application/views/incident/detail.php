<?php if( $this->session->flashdata('incidentMsgConcluir') )
{
  echo '<div class="text-warning" style="width: 80%;border: 1px solid #ccc; padding: 10px; margin-bottom: 20px">';
   echo $this->session->flashdata('incidentMsgConcluir');
  echo '</div>';
}?>
<?php if( $this->session->flashdata('incidentMsgSustentar') )
{
  echo '<div class="text-info" style="width: 80%;border: 1px solid #ccc; padding: 10px; margin-bottom: 20px">';
   echo $this->session->flashdata('incidentMsgSustentar');
  echo '</div>';
}?>
<table>
  <thead>
    <tr>
      <th colspan="2" width="20">Detalle de incidencia</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Cod.</td>
      <td><?php echo $param_object->code; ?> - <?php if ($param_object->status == 'CLOSE') {
        echo '<button class="button danger" type="button">Cerrado</button>';
        } else {
        echo '<button class="button success" type="button">Abierto</button>';
        } ?>
      </td>
    </tr>
    <tr>
      <td>Fecha</td>
      <td><?php echo date("Y-m-d", strtotime($param_object->incidence_date)); ?></td>
    </tr>
    <tr>
      <td>Usuario:</td>
      <td><?php echo $user_object["full_name"] ?></td>
    </tr>
    <tr>
      <td>Gestor:</td>
      <td><?php echo $gestor_object["full_name"] ?></td>
    </tr>
    <tr>
      <td>Sistem.</td>
      <td><?php echo $param_object->system; ?></td>
    </tr>
    <tr>
      <td>Entidad.</td>
      <td><?php echo $param_object->entity; ?></td>
    </tr>
    <tr>
      <td>Casuistica</td>
      <td><?php echo $param_object->causes; ?></td>
    </tr>
    <tr>
      <td colspan="2">
        <?php if ($is_gestor) { ?>
        <a href="<?php echo FULL_BASE_URL."/incident/sustentar/".$param_object->id; ?>"
          class="button primary">Sustentar</a>
        <?php } ?>
        <?php if ($is_analista) { ?>
        <?php if ($param_object->status == 'CLOSE') {?>
        <?php echo "Esta incidencia esta cerrada"; ?>
        <?php } else { ?>
        <a href="<?php echo FULL_BASE_URL."/incident/concluir/".$param_object->id; ?>"
          class="button danger">Conclución</a>
        <?php } ?>
        <?php } ?>
      </td>
    </tr>
    <?php if ($param_object->status == 'CLOSE') {?>
    <?php if ($sustent_object != null  ) {?>
    <tr>
      <td>Estado del usuario</td>
      <td>
        <?php if( $sustent_object->user_status == 'UNBLOCKED'){?>
        Desbloqueado
        <?php } else { ?>
        Bloqueado
        <?php } ?>
      </td>
    </tr>
    <tr>
      <td>Sustento</td>
      <td>
        <?php echo $sustent_object->sustent;?>
      </td>
    </tr>
    <tr>
      <td>Conclusion</td>
      <td>
        <?php echo $sustent_object->conclusion;?>
      </td>
    </tr>
    <?php } ?>
    <?php } ?>
    <tr>
      <td colspan="2">
        <a href="<?php echo FULL_BASE_URL.'/incident/index'; ?>" class="button info">Atras</a>
      </td>
    </tr>
  </tbody>
</table>