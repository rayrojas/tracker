<?php if( $this->session->flashdata('incidentMsgConcluir') )
{
  echo '<div class="text-warning" style="width: 80%;border: 1px solid #ccc; padding: 10px; margin-bottom: 20px">';
   echo $this->session->flashdata('incidentMsgConcluir');
  echo '</div>';
}?>
<?php if( $this->session->flashdata('incidentMsgSustentar') )
{
  echo '<div class="text-info" style="width: 80%;border: 1px solid #ccc; padding: 10px; margin-bottom: 20px">';
   echo $this->session->flashdata('incidentMsgSustentar');
  echo '</div>';
}?>
<?php echo form_open('incident/concluir_db'); ?>
  <table>
    <thead>
      <tr>
        <th colspan="2" width="20">Detalle de incidencia - Conclución</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Cod.</td>
        <td><?php echo $param_object->code; ?> - <?php if ($param_object->status == 'CLOSE') {
          echo '<button class="button danger" type="button">Cerrado</button>';
          } else {
          echo '<button class="button success" type="button">Abierto</button>';
          } ?>
        </td>
      </tr>
      <tr>
        <td>Fecha</td>
        <td><?php echo date("Y-m-d", strtotime($param_object->incidence_date)); ?></td>
      </tr>
      <tr>
        <td>Usuario:</td>
        <td><?php echo $user_object["full_name"] ?></td>
      </tr>
      <tr>
        <td>Gestor:</td>
        <td><?php echo $gestor_object["full_name"] ?></td>
      </tr>
      <tr>
        <td>Sistem.</td>
        <td><?php echo $param_object->system; ?></td>
      </tr>
      <tr>
        <td>Entidad</td>
        <td><?php echo $param_object->entity; ?></td>
      </tr>
      <tr>
        <td>Casuistica</td>
        <td><?php echo $param_object->causes; ?></td>
      </tr>
      <tr>
        <td>Sustento del analista</td>
        <td><?php echo $sustent_object->sustent; ?></td>
      </tr>
      <tr>
        <td colspan="2">
          <label for="">Estado del usuario</label>
          <select required name="user_status">
            <option value="BLOCKED">Bloqueado</option>
            <option value="UNBLOCKED">Desbloqueado</option>
          </select>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <input type="hidden" name="id" value="<?php echo $param_object->id; ?>"/>
          <input type="hidden" name="detail_id" value="<?php echo $sustent_object->id; ?>"/>
          <label for="">Conclusion</label>
          <textarea name="conclusion" required></textarea>
          <div style="text-align: right">
            <button class="button primary" type="submit">Cerrar Incidencia</button>
          </div>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <a href="<?php echo FULL_BASE_URL.'/incident/index'; ?>" class="button info">Atras</a>
        </td>
      </tr>
    </tbody>
  </table>
<?php echo form_close();?>