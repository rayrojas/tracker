<?php if( $this->session->flashdata('generalError') )
{
  echo '<div class="text-warning" style="width: 80%;border: 1px solid #ccc; padding: 10px; margin-bottom: 20px">';
   echo $this->session->flashdata('generalError');
  echo '</div>';
}?>
<table>
  <tbody>
    <tr>
      <td style="text-align: center"><h1>Error 404</h1></td>
    </tr>
    <tr>
      <td style="text-align: center">
        <a href="<?php echo FULL_BASE_URL.'/incident/index'; ?>">Volver al inicio</a>
      </td>
    </tr>
  </tbody>
</table>