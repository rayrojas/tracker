CREATE TABLE `auth_group` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

CREATE TABLE `auth_user` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `password` varchar(128) COLLATE utf8_spanish2_ci NOT NULL,
   `is_superuser` tinyint(1) NOT NULL,
   `username` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
   `first_name` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
   `last_name` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
   `email` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
   `entity` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
   `is_active` tinyint(1) NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

CREATE TABLE `auth_user_groups` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`user_id` int(11) NOT NULL,
`group_id` int(11) NOT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `user_id` (`user_id`,`group_id`),
KEY `auth_user_groups_6340c63c` (`user_id`),
KEY `auth_user_groups_5f412f9a` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

CREATE TABLE `incident` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`code` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
`system` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
`entity` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
`causes` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
`incidence_date` datetime NOT NULL,
`user_staff_id` int(11) NOT NULL,
`user_gestor_id` int(11) NOT NULL,
`status` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
PRIMARY KEY (`id`),
KEY `incident_8ae70e6f` (`user_staff_id`),
KEY `incident_1a570e1f` (`user_gestor_id`),
CONSTRAINT `user_staff_id_82542e42` FOREIGN KEY (`user_staff_id`) REFERENCES `auth_user` (`id`),
CONSTRAINT `user_gestor_id_7430e366` FOREIGN KEY (`user_gestor_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

CREATE TABLE `incident_detail` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`incident_id` int(11) NOT NULL,
`user_analista_id` int(11) NOT NULL,
`sustent` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
`conclusion` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
`user_status` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
`record_date` datetime NOT NULL,
PRIMARY KEY (`id`),
KEY `incident_detail_s_6ac13c` (`incident_id`),
KEY `incident_detail_s_64c21a` (`user_analista_id`),
CONSTRAINT `incident_id_id_82542e42` FOREIGN KEY (`incident_id`) REFERENCES `incident` (`id`),
CONSTRAINT `user_analista_id_7430e366` FOREIGN KEY (`user_analista_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

------
------
------
------
insert into auth_user(id, password, is_superuser, username, first_name, last_name, email, entity, is_active) \
  values(1, '123456', 1, 'gestor', 'Juanito', 'Alcachofa', 'admin@sistemas.com', 'entidad1', 1);
insert into auth_user(id, password, is_superuser, username, first_name, last_name, email, entity, is_active) \
  values(2, '123456', 1, 'analista', 'Juanito', 'Alcachofa', 'admin@sistemas.com', 'entidad2', 1);
insert into auth_user(id, password, is_superuser, username, first_name, last_name, email, entity, is_active) \
  values(3, '123456', 1, 'staff1', 'Juanito', 'Alcachofa', 'admin@sistemas.com', 'entidad3', 1);  
insert into auth_user(id, password, is_superuser, username, first_name, last_name, email, entity, is_active) \
  values(4, '123456', 1, 'staff2', 'Juanito', 'Alcachofa', 'admin@sistemas.com', 'entidad4', 1);    
insert into auth_user(id, password, is_superuser, username, first_name, last_name, email, entity, is_active) \
  values(5, '123456', 1, 'staff3', 'Juanito', 'Alcachofa', 'admin@sistemas.com', 'entidad5', 1);      
insert into auth_user(id, password, is_superuser, username, first_name, last_name, email, entity, is_active) \
  values(6, '123456', 1, 'gestor2', 'Gesto2', 'Alcachofa', 'admin@sistemas.com', 'entidad2', 1);
  
insert into auth_group(id, name) \
  values('1', 'gestor');
insert into auth_group(id, name) \
  values('2', 'analista');
insert into auth_group(id, name) \
  values('3', 'staff');

insert into auth_user_groups(id, user_id, group_id) \
   values(1, 1, 1);
insert into auth_user_groups(id, user_id, group_id) \
   values(2, 2, 2);
insert into auth_user_groups(id, user_id, group_id) \
   values(3, 3, 3);
insert into auth_user_groups(id, user_id, group_id) \
   values(4, 4, 3);
insert into auth_user_groups(id, user_id, group_id) \
   values(5, 5, 3);

insert into incident(id, code, system, entity, causes , incidence_date, user_staff_id, user_gestor_id, status)\
   values (5, 'ABD1', 'come', 'entidad1', 'adasdasdasd', '2012-12-12', 4, 1, 'OPEN');

insert into incident(id, code, system,entity,causes , incidence_date, user_staff_id, user_gestor_id, status)\
   values (6, 'AB21', 'come', 'entidad2','adasdasdasd', '2012-12-12', 5, 1, 'CLOSE');

insert into incident(id, code, system,entity, causes, incidence_date, user_staff_id, user_gestor_id, status)\
   values (7, 'ABD1', 'come', 'entidad2', 'adasdasdasd','2012-12-12', 6, 6, 'OPEN');
   
insert into incident_detail(incident_id, user_analista_id, sustent, conclusion, user_status, record_date)\
   values (5, 5,  'saasdasd', 'asdadas', 'BLOCKED', '2017-12-12');
